#pragma warning disable 1591
namespace UmbracoMvcBlog.Domain.Constants
{
	public static partial class PackageConstants
	{
		public const string ApplicationName = "UmbracoMvcBlog";
		public const string FaviconResourcePath = "UmbracoMvcBlog.Core.Resources.Images.favicon.ico";
		public const string IconResourcePath = "uComponents.Core.Resources.Images.icon.png";

		public const string TestAppSetting = "TestAppSetting";
	}
}
#pragma warning restore 1591
