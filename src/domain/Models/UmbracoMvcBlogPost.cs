﻿namespace UmbracoMvcBlog.Domain.Models
{
	using Vega.USiteBuilder;

	[DocumentType(Name = "UmbracoMvcBlog - Post", 
								IconUrl = "docPic.gif", 
								Thumbnail = "docWithImage.png")]
	public class UmbracoMvcBlogPost : DocumentTypeBase
	{
		[DocumentTypeProperty(UmbracoPropertyType.Textstring, Mandatory = true, Tab="Content")]
		public string PostTitle { get; set; }
	}
}
