﻿// ReSharper disable InconsistentNaming
namespace UmbracoMvcBlog.Tests.Unit.Umbraco.Abstraction.Services
{
	using System;
	using Moq;
	using NUnit.Framework;
	using UmbracoMvcBlog.Umbraco.Abstraction.Adapters;
	using UmbracoMvcBlog.Umbraco.Abstraction.Services;
	using UmbracoMvcBlog.Umbraco.Abstraction.Services.Umbraco;

	[TestFixture]
	public class ContentManipulationServiceFixture
	{
		private IContentManipulationService Service { get; set; }
		private Mock<IDocumentAdapter> MockDocumentAdapter { get; set; }

		[SetUp]
		public void Setup()
		{
			MockDocumentAdapter = new Mock<IDocumentAdapter>();
			Service = new ContentManipulationService(MockDocumentAdapter.Object);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void Constructor_Throws_Exception_When_No_Document_Wrapper_Passed()
		{
			
			Service = new ContentManipulationService(null);

			Assert.Fail();
		}

		[Test]
		public void GetById_Returns_DocumentAdapter_For_Given_Document_Id()
		{
			//Arrange
			const int documentId = 1;
			MockDocumentAdapter.Setup(m => m.LoadDocument(It.IsAny<int>())).Verifiable();

			//Act
			var result = Service.GetById(documentId);

			//Assert
			Assert.IsInstanceOf<IDocumentAdapter>(result);
			MockDocumentAdapter.Verify(m => m.LoadDocument(It.IsAny<int>()));
		}

		[Test]
		public void Save_Calls_Save_On_Document_Adapter()
		{
			//Arrange
			MockDocumentAdapter.Setup(m => m.Save()).Verifiable();

			//Act
			Service.Save(MockDocumentAdapter.Object);

			//Assert
			MockDocumentAdapter.Verify(m => m.Save());
		}

		[Test]
		public void Save_And_Publish_Calls_Save_And_Publish_On_Document_Adapter()
		{
			//Arrange
			MockDocumentAdapter.Setup(m => m.SaveAndPublish(It.IsAny<int>(),It.IsAny<bool>())).Verifiable();

			//Act
			Service.SaveAndPublish(MockDocumentAdapter.Object,1,true);

			//Assert
			MockDocumentAdapter.Verify(m => m.SaveAndPublish(1, true));
		}

		[TearDown]
		public void Teardown()
		{
			MockDocumentAdapter = null;
			Service = null;
		}
	}
}
// ReSharper restore InconsistentNaming