﻿// ReSharper disable InconsistentNaming
namespace UmbracoMvcBlog.Tests.Unit.Umbraco.Abstraction.Services
{
	using System.Collections.Generic;
	using System.Linq;
	using Moq;
	using NUnit.Framework;
	using UmbracoMvcBlog.Umbraco.Abstraction.Adapters;
	using UmbracoMvcBlog.Umbraco.Abstraction.Services;
	using UmbracoMvcBlog.Umbraco.Abstraction.Services.Umbraco;
	using umbraco.NodeFactory;
	using umbraco.cms.businesslogic.media;
	using umbraco.interfaces;

	[TestFixture]
	public class ContentReaderServiceFixture
	{
		private IContentReaderService Service { get; set; }
		private Mock<INodeAdapter> MockNodeAdapter { get; set; }
		private Mock<ITagAdapter> MockTagAdapter { get; set; }
		private Mock<IMediaAdapter> MockMediaAdapter { get; set; }
		private Mock<ILibraryAdapter> MockLibraryAdapter { get; set; }

		[SetUp]
		public void Setup()
		{
			MockNodeAdapter = new Mock<INodeAdapter>();
			MockTagAdapter = new Mock<ITagAdapter>();
			MockMediaAdapter = new Mock<IMediaAdapter>();
			MockLibraryAdapter = new Mock<ILibraryAdapter>();
			Service = new ContentReaderService(MockNodeAdapter.Object, MockTagAdapter.Object, MockMediaAdapter.Object,
																				MockLibraryAdapter.Object);

		}

		[Test]
		public void GetCurrentItem_Returns_Expected_INode()
		{
			//Arrange
			const int expectedId = 1;
			var mockNode = new Mock<INode>();
			mockNode.Setup(m => m.Id).Returns(expectedId);

			MockNodeAdapter.Setup(m => m.GetCurrent()).Returns(mockNode.Object);

			//Act
			var result = Service.GetCurrentItem();

			//Assert
			Assert.AreEqual(result.Id, expectedId);
		}


		[Test]
		public void GetCurrentPageId_Returns_Expected_Id()
		{
			//Arrange
			const int expectedId = 1;
			var mockNode = new Mock<INode>();
			mockNode.Setup(m => m.Id).Returns(expectedId);
			MockNodeAdapter.Setup(m => m.GetCurrent()).Returns(mockNode.Object);

			//Act
			var result = Service.GetCurrentPageId();

			//Asert
			Assert.AreEqual(result, expectedId);
		}

		[Test]
		public void GetAllTagsInGroup_Returns_Expected_Tags()
		{
			//Arrange
			const string expectGroupName = "test";
			var mockTag1 = new Mock<ITag>();
			mockTag1.Setup(m => m.Group).Returns(expectGroupName);

			var mockTag2 = new Mock<ITag>();
			mockTag2.Setup(m => m.Group).Returns(expectGroupName);

			MockTagAdapter.Setup(m => m.GetTags(It.IsAny<string>())).Returns(new List<ITag> {mockTag1.Object, mockTag2.Object});

			//Act
			var result = Service.GetAllTagsInGroup(expectGroupName);

			//Assert
			Assert.IsInstanceOf<IEnumerable<ITag>>(result);
			Assert.AreEqual(result.Count, 2);

		}

		[Test]
		public void GetHomePageNode_Returns_Expected_Node()
		{
			//Arrange
			var mockContentNode = new Mock<INode>();
			mockContentNode.Setup(m => m.Id).Returns(-1);

			const string expectedName = "homepage";
			var mockNode = new Mock<INode>();
			mockNode.Setup(m => m.Name).Returns(expectedName);

			mockContentNode.Setup(m => m.ChildrenAsList).Returns(new List<INode> {mockNode.Object});


			MockNodeAdapter.Setup(m => m.Get()).Returns(mockContentNode.Object);

			//Act
			var result = Service.GetHomePageNode();

			//Assert
			Assert.AreEqual(expectedName, result.Name);

		}

		[Test]
		public void GetMediItem_Returns_Expected_Media()
		{
			//Arrange
			var mockMedia = new Mock<Media>(1);
			mockMedia.Setup(m => m.Text).Returns("test");

			MockMediaAdapter.Setup(m => m.Get(It.IsAny<int>())).Returns(mockMedia.Object);

			//Act
			var result = Service.GetMediaItem(1);

			//Assert
			Assert.IsInstanceOf<Media>(result);

		}

		[Test]
		public void GetNiceUrl_Returns_Expected_Url()
		{
			//Arrange
			const string expectedUrl = "/test";
			MockLibraryAdapter.Setup(m => m.NiceUrl(It.IsAny<int>())).Returns(expectedUrl);

			//Act
			var result = Service.GetNiceUrl(1);

			//Assert
			Assert.AreEqual(expectedUrl, result);
		}

		[Test]
		public void GetFirstParentByAlias_Returns_Expected_Parent()
		{
			//Arrange
			const int expectedNodeId = 1;

			var mockParentNode = new Mock<INode>();
			mockParentNode.Setup(m => m.Id).Returns(expectedNodeId);

			var mockChildNode = new Mock<INode>();
			mockChildNode.Setup(m => m.NodeTypeAlias).Returns("test");
			mockChildNode.Setup(m => m.Parent).Returns(mockParentNode.Object);
			mockChildNode.Setup(m => m.Id).Returns(0);
			MockNodeAdapter.Setup(m => m.GetCurrent()).Returns(mockChildNode.Object);
			MockNodeAdapter.Setup(m => m.Get()).Returns(mockChildNode.Object);

			//Act
			var result = Service.GetFirstParentByAlias("test");

			//Assert
			Assert.AreEqual(expectedNodeId, result.Id);

		}

		[Test]
		public void GetFirstParentByAlias_Returns_Null_When_Parent_Not_Found()
		{
			//Arrange
			var mockChildNode = new Mock<INode>();
			mockChildNode.Setup(m => m.NodeTypeAlias).Returns("test");
			mockChildNode.Setup(m => m.Id).Returns(0);
			MockNodeAdapter.Setup(m => m.GetCurrent()).Returns(mockChildNode.Object);
			MockNodeAdapter.Setup(m => m.Get()).Returns(mockChildNode.Object);

			//Act
			var result = Service.GetFirstParentByAlias("test");

			//Assert
			Assert.IsNull(result);

		}

		[Test]
		public void GetSiblings_Returns_Expected_Node()
		{
			//Arrange
			const int expectedSiblingNodeId = 1;
			const int otherSiblingNodeId = 2;
			var mockChildNode1 = new Mock<INode>();
			mockChildNode1.Setup(m => m.Id).Returns(expectedSiblingNodeId);

			var mockChildNode2 = new Mock<INode>();
			mockChildNode2.Setup(m => m.Id).Returns(otherSiblingNodeId);

			var mockParentNode = new Mock<INode>();
			mockParentNode.Setup(m => m.ChildrenAsList).Returns(new List<INode> {mockChildNode1.Object, mockChildNode2.Object});
			mockChildNode2.Setup(m => m.Parent).Returns(mockParentNode.Object);

			MockNodeAdapter.Setup(m => m.Get()).Returns(mockChildNode2.Object);

			//Act
			var result = Service.GetSiblings(otherSiblingNodeId);

			//Assert
			Assert.AreEqual(expectedSiblingNodeId,result.ToList().First().Id);
		}

		[Test]
		public void GetSiblings_Returns_Empty_List_When_No_Siblings()
		{
			//Arrange
			const int expectedSiblingNodeId = 1;
			var mockChildNode1 = new Mock<INode>();
			mockChildNode1.Setup(m => m.Id).Returns(expectedSiblingNodeId);

			var mockParentNode = new Mock<INode>();
			mockParentNode.Setup(m => m.ChildrenAsList).Returns(new List<INode> { mockChildNode1.Object });
			mockChildNode1.Setup(m => m.Parent).Returns(mockParentNode.Object);

			MockNodeAdapter.Setup(m => m.Get()).Returns(mockChildNode1.Object);

			//Act
			var result = Service.GetSiblings(expectedSiblingNodeId);

			//Assert
			Assert.AreEqual(0, result.Count());
		}

		[Test]
		public void GetSiblings_Filtered_By_Node_Type_Alias_Returns_Expected_Node()
		{
			//Arrange
			const string nodeTypeAlias = "test";
			const int expectedSiblingNodeId = 1;
			const int otherSiblingNodeId = 2;
			var mockChildNode1 = new Mock<INode>();
			mockChildNode1.Setup(m => m.Id).Returns(expectedSiblingNodeId);
			mockChildNode1.Setup(m => m.NodeTypeAlias).Returns(nodeTypeAlias);

			var mockChildNode2 = new Mock<INode>();
			mockChildNode2.Setup(m => m.Id).Returns(otherSiblingNodeId);
			mockChildNode2.Setup(m => m.NodeTypeAlias).Returns(nodeTypeAlias);

			var mockParentNode = new Mock<INode>();
			mockParentNode.Setup(m => m.ChildrenAsList).Returns(new List<INode> { mockChildNode1.Object, mockChildNode2.Object });
			mockChildNode2.Setup(m => m.Parent).Returns(mockParentNode.Object);

			MockNodeAdapter.Setup(m => m.Get()).Returns(mockChildNode2.Object);

			//Act
			var result = Service.GetSiblings(otherSiblingNodeId, nodeTypeAlias);

			//Assert
			Assert.AreEqual(expectedSiblingNodeId, result.ToList().First().Id);
		}

		[Test]
		public void GetSiblings_Filtered_By_Node_Type_Alias_Returns_Empty_List_When_Node_Type_Not_Found()
		{
			//Arrange
			const string nodeTypeAlias = "test";
			const int expectedSiblingNodeId = 1;
			const int otherSiblingNodeId = 2;
			var mockChildNode1 = new Mock<INode>();
			mockChildNode1.Setup(m => m.Id).Returns(expectedSiblingNodeId);
			mockChildNode1.Setup(m => m.NodeTypeAlias).Returns(nodeTypeAlias);

			var mockChildNode2 = new Mock<INode>();
			mockChildNode2.Setup(m => m.Id).Returns(otherSiblingNodeId);
			mockChildNode2.Setup(m => m.NodeTypeAlias).Returns(nodeTypeAlias);

			var mockParentNode = new Mock<INode>();
			mockParentNode.Setup(m => m.ChildrenAsList).Returns(new List<INode> { mockChildNode1.Object, mockChildNode2.Object });
			mockChildNode2.Setup(m => m.Parent).Returns(mockParentNode.Object);

			MockNodeAdapter.Setup(m => m.Get()).Returns(mockChildNode2.Object);

			//Act
			var result = Service.GetSiblings(otherSiblingNodeId, "wrong");

			//Assert
			Assert.AreEqual(0, result.Count());
		}

		[Test]
		public void GetParentNodesForCurrentItem_Returns_Expected_Nodes_For_Current_Item()
		{
			//Arrange
			const int grandParentNodeId = 1;
			const int parentNodeId = 2;
			const int childNodeId = 3;

			var mockChildNode = new Mock<INode>();
			mockChildNode.Setup(m => m.Id).Returns(childNodeId);

			var mockParentNode = new Mock<INode>();
			mockParentNode.Setup(m => m.Id).Returns(parentNodeId);
			mockParentNode.Setup(m => m.ChildrenAsList).Returns(new List<INode> {mockChildNode.Object});
			mockChildNode.Setup(m => m.Parent).Returns(mockParentNode.Object);

			var mockGrandParentNode = new Mock<INode>();
			mockGrandParentNode.Setup(m => m.Id).Returns(grandParentNodeId);
			mockGrandParentNode.Setup(m => m.ChildrenAsList).Returns(new List<INode> {mockParentNode.Object});
			mockParentNode.Setup(m => m.Parent).Returns(mockGrandParentNode.Object);


			MockNodeAdapter.Setup(m => m.GetCurrent()).Returns(mockChildNode.Object);

			//Act
			var result = Service.GetParentNodesForCurrentItem("test");

			//Assert
			Assert.AreEqual(2, result.Count());
			Assert.AreEqual(1, result.ToList().First().Id);
			Assert.AreEqual(2, result.ToList().Last().Id);
		}




		[TearDown]
		public void Teardown()
		{
			MockNodeAdapter = null;
			Service = null;
		}
	}


}

// ReSharper restore InconsistentNaming