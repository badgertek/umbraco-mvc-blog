﻿namespace UmbracoMvcBlog.Cache.Services.Impl
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Net;
	using Data;

	public class ApplicationCacheService : IApplicationCacheService
	{

		private readonly IApplicationCacheRepository _applicationCacheRepository;

		public ApplicationCacheService(IApplicationCacheRepository applicationCacheRepository)
		{
			if (applicationCacheRepository == null)
				throw new ArgumentException("applicationCacheRepository");

			_applicationCacheRepository = applicationCacheRepository;
		}

		public void CreateCache(string cacheName, string key, object value)
		{
			_applicationCacheRepository.CreateCache(cacheName, key, value);
		}

		public void AddToCache(string cacheName, string key, object value)
		{
			_applicationCacheRepository.AddToCache(cacheName, key, value);
		}

		public bool HasCacheValue(string cacheName, string key)
		{
			return _applicationCacheRepository.HasCacheValue(cacheName, key);
		}

		public object GetCacheValue(string cacheName, string key)
		{
			return _applicationCacheRepository.GetCacheValue(cacheName, key);
		}

		public void ClearCaches()
		{
			_applicationCacheRepository.ClearCaches();
		}

		public void ClearDistributedCache(string url)
		{
			var request = (HttpWebRequest) WebRequest.Create(url);
			var response = (HttpWebResponse) request.GetResponse();
			var stream = new StreamReader(response.GetResponseStream());
			var responseText = stream.ReadToEnd();

			if(responseText != "ok")
				throw new Exception(string.Format("Unable to clear remote cache on {0}, response returned was: {1}", url, responseText));

		}

		public Dictionary<string, object> GetAllCacheValues(string cacheName)
		{
			return _applicationCacheRepository.GetAllCacheValues(cacheName);
		}

	}
}
