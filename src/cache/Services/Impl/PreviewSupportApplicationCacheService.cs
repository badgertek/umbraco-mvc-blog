﻿namespace UmbracoMvcBlog.Cache.Services.Impl
{
	using System.Collections.Generic;

	public class PreviewSupportApplicationCacheService : IApplicationCacheService
	{
		public void CreateCache(string cacheName, string key, object value)
		{ }

		public void AddToCache(string cacheName, string key, object value)
		{ }

		public bool HasCacheValue(string cacheName, string key)
		{
			return false;
		}

		public object GetCacheValue(string cacheName, string key)
		{
			return null;
		}

		public void ClearCaches()
		{ }

		public void ClearDistributedCache(string url)
		{ }

		public Dictionary<string, object> GetAllCacheValues(string cacheName)
		{
			return new Dictionary<string, object>();
		}

	}
}