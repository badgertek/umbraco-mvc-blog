﻿namespace UmbracoMvcBlog.Cache.Services
{
	using System.Collections.Generic;

	public interface IApplicationCacheService
	{
		void CreateCache(string cacheName, string key, object value);
		void AddToCache(string cacheName, string key, object value);
		bool HasCacheValue(string cacheName, string key);
		object GetCacheValue(string cacheName, string key);
		void ClearCaches();
		void ClearDistributedCache(string url);
		Dictionary<string, object> GetAllCacheValues(string cacheName);
	}
}
