﻿namespace UmbracoMvcBlog.Cache.Data.Impl
{
	using System.Collections.Generic;
	using System.Linq;

	public class ApplicationCacheRepository : IApplicationCacheRepository
	{
		private readonly object _syncLock;
		private Dictionary<string, Dictionary<string, object>> _cache;

		public ApplicationCacheRepository()
		{
			_cache = new Dictionary<string, Dictionary<string, object>>();
			_syncLock = new object();
		}

		public void CreateCache(string cacheName, string key, object value)
		{
			lock (_syncLock)
			{
				if (_cache.ContainsKey(cacheName))
					_cache.Remove(cacheName);

				_cache.Add(cacheName, new Dictionary<string, object>());

				_cache[cacheName].Add(key, value);
			}
		}

		public void AddToCache(string cacheName, string key, object value)
		{
			lock (_syncLock)
			{
				if (!_cache.ContainsKey(cacheName))
					_cache.Add(cacheName, new Dictionary<string, object>());

				Dictionary<string, object> itemCache = _cache[cacheName];

				if (itemCache.ContainsKey(key))
					itemCache[key] = value;
				else
					itemCache.Add(key, value);
			}
		}

		public bool HasCacheValue(string cacheName, string key)
		{
			if (!_cache.ContainsKey(cacheName)) return false; //No cache configured with that name

			Dictionary<string, object> itemCache = _cache[cacheName];

			return itemCache.ContainsKey(key);
		}

		public object GetCacheValue(string cacheName, string key)
		{
			if (!_cache.ContainsKey(cacheName)) return null; //No cache configured with that name

			Dictionary<string, object> itemCache = _cache[cacheName];

			if (!itemCache.ContainsKey(key)) return null; //Cache found but this item does not exist

			return itemCache[key];
		}

		public void ClearCaches()
		{
			lock (_syncLock)
			{
				_cache = new Dictionary<string, Dictionary<string, object>>();
			}
		}

		public Dictionary<string, object> GetAllCacheValues(string cacheName)
		{
			if (!_cache.ContainsKey(cacheName)) return null;

			return _cache[cacheName];
		}

		public List<string> GetActiveCacheNames()
		{
			lock (_syncLock)
			{
				return _cache.Select(x => x.Key).ToList();
			}
		}

	}
}
