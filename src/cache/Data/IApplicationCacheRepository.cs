﻿namespace UmbracoMvcBlog.Cache.Data
{
	using System;
	using System.Collections.Generic;

	public interface IApplicationCacheRepository
	{
		void CreateCache(string cacheName, string key, object value);
		void AddToCache(string cacheName, string key, object value);
		bool HasCacheValue(string cacheName, string key);
		object GetCacheValue(string cacheName, string key);
		void ClearCaches();
		Dictionary<string, object> GetAllCacheValues(string cacheName);
		List<String> GetActiveCacheNames();
	}
}
