﻿namespace UmbracoMvcBlog.Installer.PackageActions
{
	using System.Configuration;
	using System.Web.Configuration;
	using System.Xml;
	using Domain.Constants;
	using umbraco.interfaces;
	using umbraco.cms.businesslogic.packager.standardPackageActions;


	/// <summary>
	/// Adds a key to the web.config app settings.
	/// </summary>
	/// <remarks>
	/// This package action has been adapted from the PackageActionsContrib Project.
	/// http://packageactioncontrib.codeplex.com
	/// </remarks>
	public class AddAssemblyBindings : IPackageAction
	{
		/// <summary>
		/// The alias of the action - for internal use only.
		/// </summary>
		internal static readonly string ActionAlias = string.Concat(PackageConstants.ApplicationName, "_AddAssemblyBindings");

		internal string Name { get; set; }
		internal string PublicKeyToken { get; set; }
		internal string Culture { get; set; }
		internal string OldVersion { get; set; }
		internal string NewVersion { get; set; }
		internal XmlDocument WebConfigXmlDocument { get; set; }

		/// <summary>
		/// This Alias must be unique and is used as an identifier that must match the alias in the package action XML.
		/// </summary>
		/// <returns>The Alias of the package action.</returns>
		public string Alias()
		{
			return ActionAlias;
		}

		/// <summary>
		/// Adds an appSettings key to the web.config file.
		/// </summary>
		/// <param name="packageName">Name of the package that we install.</param>
		/// <param name="xmlData">The XML data for the appSettings key.</param>
		/// <returns>True when succeeded.</returns>
		public bool Execute(string packageName, XmlNode xmlData)
		{
			try
			{
				Name = xmlData.Attributes["name"].Value;
				PublicKeyToken = xmlData.Attributes["publicKeyToken"].Value;
				Culture = xmlData.Attributes["culture"].Value;
				OldVersion = xmlData.Attributes["oldVersion"].Value;
				NewVersion = xmlData.Attributes["newVersion"].Value;


				CreateAssemblyBinding();

				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Provides a sample of the XML.
		/// </summary>
		/// <returns></returns>
		public XmlNode SampleXml()
		{
			var sample = string.Concat("<Action runat=\"install\" undo=\"true/false\" alias=\"", ActionAlias, "\" name=\"name of assembly\" publicKeyToken=\"public key token of the assembly\" culture=\"culture of the assembly\" oldVersion=\"old version of the assembly\" newVersion=\"new/target version of the assembly\" />");
			return helper.parseStringToXmlNode(sample);
		}

		/// <summary>
		/// Removes an appSettings key to the web.config file.
		/// </summary>
		/// <param name="packageName">Name of the package that we install.</param>
		/// <param name="xmlData">The XML data for the appSettings key.</param>
		/// <returns>True when succeeded.</returns>
		public bool Undo(string packageName, XmlNode xmlData)
		{
			try
			{
				var addKey = xmlData.Attributes["key"].Value;

				// as long as addKey has a value, remove it from the key entry in web.config
				if (addKey != string.Empty)
				{
					this.RemoveAppSettingsKey(addKey);
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private void CreateAssemblyBinding()
		{
			WebConfigXmlDocument = new XmlDocument();
			WebConfigXmlDocument.Load("~/web.config");
			var manager = new XmlNamespaceManager(WebConfigXmlDocument.NameTable);
      manager.AddNamespace("bindings", "urn:schemas-microsoft-com:asm.v1");

			XmlNode root = WebConfigXmlDocument.DocumentElement;

      XmlNode node = root.SelectSingleNode("//configuration:runtime:assemblyBinding", manager);

			node.AppendChild(GetDependentAssemblyNode());

			WebConfigXmlDocument.Save("~/web.config");


		}

		private XmlNode GetDependentAssemblyNode()
		{
			var node = WebConfigXmlDocument.CreateNode(XmlNodeType.Element, "dependentAssembly", "urn:schemas-microsoft-com:asm.v1");
			node.AppendChild(GetAssemblyIdentityNode());
			node.AppendChild(GetBindingRedirectNode());

			return node;

		}

		private XmlNode GetAssemblyIdentityNode()
		{
			var node = WebConfigXmlDocument.CreateNode(XmlNodeType.Element, "assemblyIdentity", "urn:schemas-microsoft-com:asm.v1");
			var nameAttribute = WebConfigXmlDocument.CreateAttribute("name");
			nameAttribute.Value = Name;

			var publicKeyTokenAttribute = WebConfigXmlDocument.CreateAttribute("publicKeyToken");
			publicKeyTokenAttribute.Value = PublicKeyToken;

			var cultureAtribute = WebConfigXmlDocument.CreateAttribute("culture");
			cultureAtribute.Value = Culture;

			node.Attributes.Append(nameAttribute);
			node.Attributes.Append(publicKeyTokenAttribute);
			node.Attributes.Append(cultureAtribute);

			return node;

		}

		private XmlNode GetBindingRedirectNode()
		{
			var node = WebConfigXmlDocument.CreateNode(XmlNodeType.Element, "bindingRedirect", "urn:schemas-microsoft-com:asm.v1");
			var oldVersionAttribute = WebConfigXmlDocument.CreateAttribute("oldVersion");
			oldVersionAttribute.Value = OldVersion;

			var newVersionAttribute = WebConfigXmlDocument.CreateAttribute("publicKeyToken");
			newVersionAttribute.Value = NewVersion;

			node.Attributes.Append(oldVersionAttribute);
			node.Attributes.Append(newVersionAttribute);

			return node;

		}

		/// <summary>
		/// Removes the appSettings key.
		/// </summary>
		/// <param name="key">The key.</param>
		private void RemoveAppSettingsKey(string key)
		{
			var config = WebConfigurationManager.OpenWebConfiguration("~");
			var appSettings = (AppSettingsSection)config.GetSection("appSettings");

			appSettings.Settings.Remove(key);

			config.Save(ConfigurationSaveMode.Modified);
		}
	}
}