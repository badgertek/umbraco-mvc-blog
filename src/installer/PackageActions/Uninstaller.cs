﻿#pragma warning disable 1591
namespace UmbracoMvcBlog.Installer.PackageActions
{
	using System.Collections.Generic;
	using System.Text;
	using System.Xml;
	using Domain.Constants;
	using umbraco.cms.businesslogic.packager;
	using umbraco.interfaces;

	public class Uninstaller : IPackageAction
	{

		internal static readonly string ActionAlias = string.Concat(PackageConstants.ApplicationName, "_Uninstaller");

		public bool Execute(string packageName, XmlNode xmlData)
		{
			//throw new System.NotImplementedException();
			return true;
		}

		public string Alias()
		{
			return ActionAlias;
		}

		public bool Undo(string packageName, XmlNode xmlData)
		{
			var sb = new StringBuilder("<Actions>");

			var appSettingsKeys = new List<string>()
			{
				PackageConstants.TestAppSetting
			};

			foreach (var appSettingsKey in appSettingsKeys)
			{
				sb.AppendFormat("<Action runat=\"install\" undo=\"true\" alias=\"{0}\" key=\"{1}\" value=\"false\" />", AddAppConfigKey.ActionAlias, appSettingsKey);
			}

			sb.Append("</Actions>");

			var actionsXml = new XmlDocument();
			actionsXml.LoadXml(sb.ToString());

			foreach (XmlNode node in actionsXml.DocumentElement.SelectNodes("//Action"))
			{
				PackageAction.UndoPackageAction(PackageConstants.ApplicationName, node.Attributes["alias"].Value, node);
			}

			return true;
		}

		public XmlNode SampleXml()
		{
			//throw new System.NotImplementedException();
			return null;
		}
	}
}
#pragma warning restore 1591

