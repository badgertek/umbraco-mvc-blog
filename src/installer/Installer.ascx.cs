﻿namespace UmbracoMvcBlog.Installer
{
	using System;
	using System.Xml;
	//using Core.Services;
	using Domain.Constants;
	using Microsoft.Practices.ServiceLocation;
	using Ninject;

	public partial class Installer : System.Web.UI.UserControl
	{

		//protected IDocumentTypeService DocumentTypeService {
		//	get { return ServiceLocator.Current.GetInstance<IDocumentTypeService>(); }
		//}

		protected void Page_Init(object sender, EventArgs e)
		{
			var xml = new XmlDocument();

			//xml.LoadXml(string.Format("<Action runat=\"install\" undo=\"true\" alias=\"UmbracoMvcBlog_AddAppConfigKey\" key=\"{0}\" value=\"{1}\" />", PackageConstants.TestAppSetting, "true"));
			//umbraco.cms.businesslogic.packager.PackageAction.RunPackageAction("foo", "UmbracoMvcBlog_AddAppConfigKey", xml.FirstChild);

			xml.LoadXml("<Action runat=\"install\" undo=\"true\" alias=\"UmbracoMvcBlog_AddAssemblyBindings\" name=\"ninject\" publicKeyToken=\"c7192dc5380945e7\" culture=\"neutral\" oldVersion=\"0.0.0.0-3.0.0.0\" newVersion=\"3.0.0.0\" />");
			umbraco.cms.businesslogic.packager.PackageAction.RunPackageAction("AddAssemblyBindings","UmbracoMvcBlog_AddAssemblyBindings", xml.FirstChild);

			//DocumentTypeService.CreateDocumentTypes();
				
		}


		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}