﻿namespace UmbracoMvcBlog.Installer.App.Ninject.Modules
{
	//using Core.Services;
	using global::Ninject.Modules;

	public class CoreModule : NinjectModule
	{
		public override void Load()
		{
			ConfigureServices();
		}

		private void ConfigureRepositories()
		{
			
		}

		private void ConfigureServices()
		{
			//Bind<ITemplateService>().To<TemplateService>().InSingletonScope();
			//Bind<IDocumentTypeService>().To<DocumentTypeService>().InSingletonScope();
		}
	}
}
