﻿namespace UmbracoMvcBlog.Installer.App.Ninject.Modules
{
	using Umbraco.Abstraction.Adapters;
	using Umbraco.Abstraction.Adapters.Umbraco;
	using Umbraco.Abstraction.Services;
	using Umbraco.Abstraction.Services.Umbraco;
	using global::Ninject.Modules;
	using global::Ninject.Web.Common;

	public class UmbracoAbstractionModule : NinjectModule
	{
		public override void Load()
		{
			ConfigureAdapters();
			ConfigureServices();
		}

		private void ConfigureAdapters()
		{
			Bind<IDocumentAdapter>().To<DocumentAdapter>().InSingletonScope();
			Bind<ILibraryAdapter>().To<LibraryAdapter>().InSingletonScope();
			Bind<IUserAdapter>().To<UserAdapter>().InSingletonScope();
			Bind<INodeAdapter>().To<NodeAdapter>().InSingletonScope();
			Bind<ITagAdapter>().To<TagAdapter>().InSingletonScope();
		}

		private void ConfigureServices()
		{
			// Content reader stack
			Bind<IContentReaderService>().To<RequestCachedContentReaderService>().InRequestScope();
			Bind<IContentReaderService>().To<ApplicationCachedContentReaderService>()
				.WhenInjectedInto<RequestCachedContentReaderService>().InSingletonScope();
			Bind<IContentReaderService>().To<ContentReaderService>()
				.WhenInjectedInto<ApplicationCachedContentReaderService>().InSingletonScope();

			Bind<IContentManipulationService>().To<ContentManipulationService>().InRequestScope();

			//Bind<IDocumentTypeManipulationService>().To<DocumentTypeManipulationService>().InSingletonScope();
			//Bind<ITemplateManipulationService>().To<TemplateManipulationService>().InSingletonScope();
		}
	}
}
