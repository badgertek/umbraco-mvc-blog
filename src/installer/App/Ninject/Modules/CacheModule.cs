﻿using Ninject.Modules;
using Ninject.Web.Common;
using UmbracoMvcBlog.Cache.Data;
using UmbracoMvcBlog.Cache.Data.Impl;
using UmbracoMvcBlog.Cache.Services;
using UmbracoMvcBlog.Installer.App.Ninject.Providers;

public class CacheModule : NinjectModule
{
	public override void Load()
	{
		// Use a provider to return the instance of IApplicationCacheService
		// The provider will return a Application Cache that forces a content 
		// reload when in preview mode - this allows the preview to work with the IContentReaderService 
		// and the renderscripts webcontrol.
		Bind<IApplicationCacheService>().ToProvider<ApplicationCacheServiceProvider>().InRequestScope();
		Bind<IApplicationCacheRepository>().To<ApplicationCacheRepository>().InSingletonScope();
	}
}