﻿namespace UmbracoMvcBlog.Installer.App.Ninject.Providers
{
	using System;
	using Cache.Data;
	using Cache.Services;
	using Cache.Services.Impl;
	using Umbraco.Abstraction.Helpers;
	using global::Ninject.Activation;

	public class ApplicationCacheServiceProvider : Provider<IApplicationCacheService>
	{
		protected override IApplicationCacheService CreateInstance(IContext context)
		{
			if (context != null && ContextHelper.InPreviewMode())
				return new PreviewSupportApplicationCacheService();

			// Get the application cached content reader service from the kernel
			// to use in the constructor of the RequestCachedContentReaderService
			var applicationCacheRepository = context.Kernel.GetService(typeof(IApplicationCacheRepository)) as IApplicationCacheRepository;

			if (applicationCacheRepository == null)
				throw new Exception("Cannot load IApplicationCacheRepository in Ninject ApplicationCacheServiceProvider");

			return new ApplicationCacheService(applicationCacheRepository);
		}
	}
}