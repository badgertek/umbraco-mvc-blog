﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Helpers
{
	public static class ContextHelper
	{

		public static bool InPreviewMode()
		{
			return umbraco.presentation.UmbracoContext.Current != null && umbraco.presentation.UmbracoContext.Current.InPreviewMode;
		}
	}
}
