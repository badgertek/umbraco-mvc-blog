﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services
{
	using System.Collections.Generic;
	using umbraco.cms.businesslogic.media;
	using umbraco.interfaces;

	public interface IContentReaderService
	{
		INode GetItemById(int id);

		INode GetCurrentItem();

		INode GetHomePageNode();

		Media GetMediaItem(int id);

		string GetNiceUrl(int nodeId);

		INode GetFirstParentByAlias(string nodeTypeAlias);

		INode GetFirstParentByAlias(string nodeTypeAlias, int nodeId);

		INode GetFirstParentByAlias(IList<string> nodeTypeAliases, int nodeId);

		int GetCurrentPageId();

		IEnumerable<int> GetParentIdsForCurrentItem(int currentNodeId);

		IEnumerable<INode> GetChildNodes(int parentNodeId);

		IEnumerable<INode> GetChildNodes(int parentNodeId, string filterByNodeAlias);

		INode GetFirstChildByAlias(string nodeTypeAlias, int nodeId);
		
		IEnumerable<INode> GetSiblings(int nodeId);

		IEnumerable<INode> GetSiblings(int nodeId, string filterByNodeAlias);

		IEnumerable<INode> GetParentNodesForCurrentItem(string hompageNodeTypeAlias);

		IEnumerable<INode> GetDescendants(INode node);

		string ParseUmbracoLocalLinks(string input);
		IList<ITag> GetAllTagsInGroup(string groupName);
	}
}
