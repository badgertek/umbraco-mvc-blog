﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services
{
	using System;
	using System.Collections.Generic;

	public interface ITemplateManipulationService
	{
		IList<Type> GetAll(Type documentType);
	}
}
