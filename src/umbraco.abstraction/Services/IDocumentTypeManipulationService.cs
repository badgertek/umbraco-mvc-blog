﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services
{
	using System;

	public interface IDocumentTypeManipulationService
	{
		void Create(Type documentType);
		void Create(Type documentType, Type baseDocumentType);
	}
}
