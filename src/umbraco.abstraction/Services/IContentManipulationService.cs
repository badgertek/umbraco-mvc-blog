﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services
{
	using Adapters;

	public interface IContentManipulationService
	{
		IDocumentAdapter GetById(int documentId);
		void Save(IDocumentAdapter documentAdapter);
		void SaveAndPublish(IDocumentAdapter documentAdapter, int userId, bool updateDocumentCache);
	}
}
