﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services.Umbraco
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using umbraco.DataLayer;
	using umbraco.cms.businesslogic.template;
	using umbraco.cms.businesslogic.web;

	public class DocumentTypeManipulationService : IDocumentTypeManipulationService
	{
		private ITemplateManipulationService _templateManipulationService;

		public DocumentTypeRepository(ITemplateManipulationService templateManipulationService)
		{
			if (templateManipulationService == null)
				throw new ArgumentNullException("templateService");


			_templateManipulationService = templateManipulationService;
		}



		public void Create(Type documentType)
		{
			Create(documentType, typeof(DocumentTypeBase));
		}

		public void Create(Type documentType, Type baseDocumentType)
		{
			DocumentTypeAttribute docTypeAttr = GetDocumentTypeAttribute(documentType);

			string documentTypeName = string.IsNullOrEmpty(docTypeAttr.Name) ? documentType.Name : docTypeAttr.Name;
			string docTypeAlias = GetDocumentTypeAlias(documentType);

			DocumentType umbDocType = DocumentType.GetByAlias(docTypeAlias);
			if (umbDocType == null)
			{
				// if name is not set, use name of the type
				umbDocType = DocumentType.MakeNew(Util.GetUmbracoAdminUser(), documentTypeName);
			}

			umbDocType.Text = documentTypeName;
			umbDocType.Alias = docTypeAlias;
			umbDocType.IconUrl = docTypeAttr.IconUrl;
			umbDocType.Thumbnail = docTypeAttr.Thumbnail;
			umbDocType.Description = docTypeAttr.Description;

			if (baseDocumentType == typeof(DocumentTypeBase))
			{
				umbDocType.MasterContentType = 0;
			}
			else
			{
				umbDocType.MasterContentType = DocumentType.GetByAlias(GetDocumentTypeAlias(baseDocumentType)).Id;
			}

			SetAllowedTemplates(umbDocType, docTypeAttr, documentType);

			//this.SynchronizeDocumentTypeProperties(typeDocType, umbDocType);

			umbDocType.Save();
		}

		private DocumentTypeAttribute GetDocumentTypeAttribute(Type typeDocType)
		{
			var retVal = Util.GetAttribute<DocumentTypeAttribute>(typeDocType);

			if (retVal == null)
			{
				retVal = CreateDefaultDocumentTypeAttribute(typeDocType);
			}

			return retVal;
		}

		private DocumentTypeAttribute CreateDefaultDocumentTypeAttribute(Type typeDocType)
		{
			var retVal = new DocumentTypeAttribute
										{
											Name = typeDocType.Name,
											IconUrl = DocumentTypeDefaultValues.IconUrl,
											Thumbnail = DocumentTypeDefaultValues.Thumbnail
										};

			return retVal;
		}

		private string GetDocumentTypeAlias(Type typeDocType)
		{
			string alias;

			DocumentTypeAttribute docTypeAttr = GetDocumentTypeAttribute(typeDocType);

			if (!String.IsNullOrEmpty(docTypeAttr.Alias))
			{
				alias = docTypeAttr.Alias;
			}
			else
			{
				alias = typeDocType.Name;
			}
			return alias;
		}

		private List<Template> GetAllowedTemplates(DocumentTypeAttribute docTypeAttr, Type typeDocType)
		{
			var allowedTemplates = new List<Template>();

			if (docTypeAttr.AllowedTemplates != null)
			{
				foreach (string templateName in docTypeAttr.AllowedTemplates)
				{
					Template template = Template.GetByAlias(templateName);
					if (template != null)
					{
						allowedTemplates.Add(template);
					}
					else
					{
						throw new Exception(string.Format("Template '{0}' does not exists. That template is set as allowed template for document type '{1}'",
								templateName, GetDocumentTypeAlias(typeDocType)));
					}
				}
			}
			else
			{
				// if AllowedTemplates if null, use all generic templates
				foreach (Type typeTemplate in _templateManipulationService.GetAll(typeDocType))
				{
					Template template = Template.GetByAlias(typeTemplate.Name);

					if (template != null)
					{
						allowedTemplates.Add(template);
					}
				}
			}

			return allowedTemplates;
		}

		private void SetAllowedTemplates(DocumentType docType, DocumentTypeAttribute docTypeAttr, Type typeDocType)
		{
			var allowedTemplates = GetAllowedTemplates(docTypeAttr, typeDocType);

			try
			{
				docType.allowedTemplates = allowedTemplates.ToArray();
			}
			catch (SqlHelperException e)
			{
				throw new Exception(string.Format("Sql error setting templates for doc type '{0}' with templates '{1}'",
						GetDocumentTypeAlias(typeDocType), string.Join(", ", allowedTemplates)));
			}

			int defaultTemplateId = GetDefaultTemplate(docTypeAttr, typeDocType, allowedTemplates);

			if (defaultTemplateId != 0)
			{
				docType.DefaultTemplate = defaultTemplateId;
			}
			else if (docType.allowedTemplates.Length == 1) // if only one template is defined for this doc type -> make it default template for this doc type
			{
				docType.DefaultTemplate = docType.allowedTemplates[0].Id;
			}
		}

		private int GetDefaultTemplate(DocumentTypeAttribute docTypeAttr, Type typeDocType, IEnumerable<Template> allowedTemplates)
		{
			var defaultTemplateId = 0;
			var defaultTemplateName = string.Empty;

			if (docTypeAttr.DefaultTemplate is string)
				defaultTemplateName = docTypeAttr.DefaultTemplate as string;
			else
				defaultTemplateName = docTypeAttr.Name;


			if (!String.IsNullOrEmpty(defaultTemplateName))
			{
				Template defaultTemplate = allowedTemplates.FirstOrDefault(t => t.Alias == defaultTemplateName);
				if (defaultTemplate == null)
				{
					throw new Exception(string.Format("Document type '{0}' has a default template '{1}' but that template does not use this document type",
							GetDocumentTypeAlias(typeDocType), defaultTemplateName));
				}

				defaultTemplateId = defaultTemplate.Id;
			}

			return defaultTemplateId;
		}
	}
}