﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services.Umbraco
{
	using System;
	using Adapters;

	public class ContentManipulationService : IContentManipulationService
	{

		private readonly IDocumentAdapter _documentAdapter;

		public ContentManipulationService(IDocumentAdapter documentAdapter)
		{
			if(documentAdapter == null)
				throw new ArgumentNullException("documentAdapter");

			_documentAdapter = documentAdapter;
		}

		public IDocumentAdapter GetById(int documentId)
		{
			_documentAdapter.LoadDocument(documentId);
			return _documentAdapter;
		}

		public void Save(IDocumentAdapter documentAdapter)
		{
			documentAdapter.Save();
		}

		public void SaveAndPublish(IDocumentAdapter documentAdapter, int userId, bool updateDocumentCache)
		{
			documentAdapter.SaveAndPublish(userId, updateDocumentCache);
		}
	}
}
