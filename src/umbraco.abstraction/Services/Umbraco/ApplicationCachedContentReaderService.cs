﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services.Umbraco
{
	using System;
	using System.Collections.Generic;
	using Cache.Services;
	using umbraco.cms.businesslogic.media;
	using umbraco.interfaces;

	public class ApplicationCachedContentReaderService : IContentReaderService
	{

		private readonly IContentReaderService _contentReaderService;
		private readonly IApplicationCacheService _applicationCacheService;

		private const string CacheName = "ApplicationCachedContentReaderService";
		private readonly object _syncLock;

		public ApplicationCachedContentReaderService(IContentReaderService contentReaderService, IApplicationCacheService applicationCacheService)
		{
			if(contentReaderService == null)
				throw new ArgumentNullException("IContentReaderService");

			if (applicationCacheService == null)
				throw new ArgumentNullException("IApplicationCacheService");

			_contentReaderService = contentReaderService;
			_applicationCacheService = applicationCacheService;
			_syncLock = new object();
		}

		#region Application Cached

		public INode GetItemById(int id)
		{
			var cacheKey = string.Format("GetItemById_{0}", id);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as INode;

			var result = _contentReaderService.GetItemById(id);

			lock(_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public INode GetHomePageNode()
		{
			var cacheKey = string.Format("GetHomePageNode");

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as INode;

			var result = _contentReaderService.GetHomePageNode();

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public Media GetMediaItem(int id)
		{
			var cacheKey = string.Format("GetMediaItem_{0}", id);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as Media;

			var result = _contentReaderService.GetMediaItem(id);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public string GetNiceUrl(int nodeId)
		{
			var cacheKey = string.Format("GetNiceUrl_{0}", nodeId);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as string;

			var result = _contentReaderService.GetNiceUrl(nodeId);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public IEnumerable<INode> GetChildNodes(int parentNodeId)
		{
			var cacheKey = string.Format("GetChildNodes_{0}", parentNodeId);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as IEnumerable<INode>;

			var result = _contentReaderService.GetChildNodes(parentNodeId);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public IEnumerable<INode> GetChildNodes(int parentNodeId, string filterByNodeAlias)
		{
			var cacheKey = string.Format("GetChildNodes_{0}_{1}", parentNodeId, filterByNodeAlias);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as IEnumerable<INode>;

			var result = _contentReaderService.GetChildNodes(parentNodeId, filterByNodeAlias);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public INode GetFirstChildByAlias(string nodeTypeAlias, int nodeId)
		{
			var cacheKey = string.Format("GetFirstChildByAlias_{0}_{1}", nodeTypeAlias, nodeId);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as INode;

			var result = _contentReaderService.GetFirstChildByAlias(nodeTypeAlias, nodeId);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public IEnumerable<INode> GetSiblings(int nodeId)
		{
			var cacheKey = string.Format("GetSiblings_{0}", nodeId);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as IEnumerable<INode>;

			var result = _contentReaderService.GetSiblings(nodeId);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public IEnumerable<INode> GetSiblings(int nodeId, string filterByNodeAlias)
		{
			var cacheKey = string.Format("GetSiblings_{0}_{1}", nodeId, filterByNodeAlias);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as IEnumerable<INode>;

			var result = _contentReaderService.GetSiblings(nodeId, filterByNodeAlias);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public IEnumerable<INode> GetDescendants(INode node)
		{
			var cacheKey = string.Format("GetDescendants_{0}", node.Id);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as IEnumerable<INode>;

			var result = _contentReaderService.GetDescendants(node);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public INode GetFirstParentByAlias(string nodeTypeAlias, int nodeId)
		{
			var cacheKey = string.Format("GetFirstParentByAlias_{0}_{1}", nodeTypeAlias, nodeId);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as INode;

			var result = _contentReaderService.GetFirstParentByAlias(nodeTypeAlias, nodeId);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public IEnumerable<int> GetParentIdsForCurrentItem(int currentNodeId)
		{
			var cacheKey = string.Format("GetParentIdsForCurrentItem_{0}", currentNodeId);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as IEnumerable<int>;

			var result = _contentReaderService.GetParentIdsForCurrentItem(currentNodeId);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}

			return result;
		}

		public IList<ITag> GetAllTagsInGroup(string groupName)
		{
			var cacheKey = string.Format("GetAllTagsInGroup_{0}", groupName);

			if (_applicationCacheService.HasCacheValue(CacheName, cacheKey))
				return _applicationCacheService.GetCacheValue(CacheName, cacheKey) as IList<ITag>;

			var result = _contentReaderService.GetAllTagsInGroup(groupName);

			lock (_syncLock)
			{
				_applicationCacheService.AddToCache(CacheName, cacheKey, result);
			}
			return result;
		}

		#endregion

		#region Non Cached

		/// <summary>
		/// Cant Cache due to parameter types.
		/// </summary>
		public INode GetFirstParentByAlias(IList<string> nodeTypeAliases, int nodeId)
		{
			return _contentReaderService.GetFirstParentByAlias(nodeTypeAliases, nodeId);
		}

		public string ParseUmbracoLocalLinks(string input)
		{
			return _contentReaderService.ParseUmbracoLocalLinks(input);
		}

		#endregion

		#region Request Cached

		public int GetCurrentPageId()
		{
			return _contentReaderService.GetCurrentPageId();
		}

		public INode GetCurrentItem()
		{
			return _contentReaderService.GetCurrentItem();
		}

		public INode GetFirstParentByAlias(string nodeTypeAlias)
		{
			return _contentReaderService.GetFirstParentByAlias(nodeTypeAlias);
		}

		public IEnumerable<INode> GetParentNodesForCurrentItem(string hompageNodeTypeAlias)
		{
			return _contentReaderService.GetParentNodesForCurrentItem(hompageNodeTypeAlias);
		}

		#endregion

	}


}
