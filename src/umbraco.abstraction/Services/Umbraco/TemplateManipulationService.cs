﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services.Umbraco
{
	using System;
	using System.Collections.Generic;

	public class TemplateManipulationService : ITemplateManipulationService
	{
		public IList<Type> GetAll(Type documentType)
		{
			var retVal = new List<Type>();

			var allTemplates = Util.GetAllSubTypes(typeof(TemplateBase));
			foreach (Type typeTemplate in allTemplates)
			{
				if (Util.IsGenericArgumentTypeOf(typeTemplate, documentType))
				{
					// try to get the attribute
					var templateAttr = Util.GetAttribute<TemplateAttribute>(typeTemplate);
					if (templateAttr == null || templateAttr.AllowedForDocumentType)
					{
						retVal.Add(typeTemplate);
					}
				}
			}

			return retVal;
		}
	}
}