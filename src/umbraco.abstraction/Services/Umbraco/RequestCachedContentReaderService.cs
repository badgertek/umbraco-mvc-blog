﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services.Umbraco
{
	using System;
	using System.Collections.Generic;
	using umbraco.cms.businesslogic.media;
	using umbraco.interfaces;

	public class RequestCachedContentReaderService : IContentReaderService
	{

		private readonly IContentReaderService _contentReaderService;
		private readonly object _syncLock;

		public RequestCachedContentReaderService(IContentReaderService contentReaderService)
		{
			if(contentReaderService == null)
				throw new ArgumentNullException("IContentReaderService");

			_contentReaderService = contentReaderService;
			_syncLock = new object();
		}

		#region Application Cached

		public INode GetItemById(int id)
		{
			return _contentReaderService.GetItemById(id);
		}

		public INode GetHomePageNode()
		{
			return _contentReaderService.GetHomePageNode();
		}

		public Media GetMediaItem(int id)
		{
			return _contentReaderService.GetMediaItem(id);
		}

		public string GetNiceUrl(int nodeId)
		{
			return _contentReaderService.GetNiceUrl(nodeId);
		}

		public IEnumerable<INode> GetChildNodes(int parentNodeId)
		{
			return _contentReaderService.GetChildNodes(parentNodeId);
		}

		public IEnumerable<INode> GetChildNodes(int parentNodeId, string filterByNodeAlias)
		{
			return _contentReaderService.GetChildNodes(parentNodeId, filterByNodeAlias);
		}

		public INode GetFirstChildByAlias(string nodeTypeAlias, int nodeId)
		{
			return _contentReaderService.GetFirstChildByAlias(nodeTypeAlias, nodeId);
		}

		public IEnumerable<INode> GetSiblings(int nodeId)
		{
			return _contentReaderService.GetSiblings(nodeId);
		}

		public IEnumerable<INode> GetSiblings(int nodeId, string filterByNodeAlias)
		{
			return _contentReaderService.GetSiblings(nodeId, filterByNodeAlias);
		}

		public IEnumerable<INode> GetDescendants(INode node)
		{
			return _contentReaderService.GetDescendants(node);
		}

		public INode GetFirstParentByAlias(string nodeTypeAlias, int nodeId)
		{
			return _contentReaderService.GetFirstParentByAlias(nodeTypeAlias, nodeId);
		}

		public INode GetFirstParentByAlias(IList<string> nodeTypeAliases, int nodeId)
		{
			return _contentReaderService.GetFirstParentByAlias(nodeTypeAliases, nodeId);
		}

		public IEnumerable<int> GetParentIdsForCurrentItem(int currentNodeId)
		{
			return _contentReaderService.GetParentIdsForCurrentItem(currentNodeId);
		}

		#endregion

		#region Non Cached

		public string ParseUmbracoLocalLinks(string input)
		{
			return _contentReaderService.ParseUmbracoLocalLinks(input);
		}

		public IList<ITag> GetAllTagsInGroup(string groupName)
		{
			return _contentReaderService.GetAllTagsInGroup(groupName);
		}

		#endregion

		#region Request Cached

		private int _currentPageId;
		public int GetCurrentPageId()
		{
			if (_currentPageId > 0)
				return _currentPageId;

			lock(_syncLock)
			{
				_currentPageId = _contentReaderService.GetCurrentPageId();
			}

			return _currentPageId;
		}

		private INode _currentItem;
		public INode GetCurrentItem()
		{
			if (_currentItem != null)
				return _currentItem;

			lock (_syncLock)
			{
				_currentItem = _contentReaderService.GetCurrentItem();
			}

			return _currentItem;
		}

		private INode _firstParentByAlias;
		public INode GetFirstParentByAlias(string nodeTypeAlias)
		{
			if (_firstParentByAlias != null)
				return _firstParentByAlias;

			lock (_syncLock)
			{
				_firstParentByAlias = _contentReaderService.GetFirstParentByAlias(nodeTypeAlias);
			}

			return _firstParentByAlias;
		}

		private IEnumerable<INode> _parentNodesForCurrentItem;
		public IEnumerable<INode> GetParentNodesForCurrentItem(string hompageNodeTypeAlias)
		{
			if (_parentNodesForCurrentItem != null)
				return _parentNodesForCurrentItem;

			lock (_syncLock)
			{
				_parentNodesForCurrentItem = _contentReaderService.GetParentNodesForCurrentItem(hompageNodeTypeAlias);
			}

			return _parentNodesForCurrentItem;
		}

		#endregion

	}
}
