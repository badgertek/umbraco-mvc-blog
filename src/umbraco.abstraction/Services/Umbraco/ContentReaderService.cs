﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Services.Umbraco
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text.RegularExpressions;
	using Adapters;
	using umbraco;
	using umbraco.cms.businesslogic.media;
	using umbraco.interfaces;

	/// <summary>
	/// Allows read only access to the published Umbraco content.
	/// </summary>
	public class ContentReaderService : IContentReaderService
	{

		private readonly INodeAdapter _nodeAdapter;
		private readonly ITagAdapter _tagAdapter;
		private readonly IMediaAdapter _mediaAdapter;
		private readonly ILibraryAdapter _libraryAdapter;

		public ContentReaderService(INodeAdapter nodeAdapter,
																ITagAdapter tagAdapter,
																IMediaAdapter mediaAdapter,
																ILibraryAdapter libraryAdapter)
		{
			if (nodeAdapter == null)
				throw new ArgumentNullException("nodeAdapter");

			if(tagAdapter == null)
				throw new ArgumentNullException("tagAdapter");

			if(mediaAdapter == null)
				throw new ArgumentNullException("mediaAdapter");

			if(libraryAdapter == null)
				throw new ArgumentNullException("libraryAdapter");


			_nodeAdapter = nodeAdapter;
			_tagAdapter = tagAdapter;
			_mediaAdapter = mediaAdapter;
			_libraryAdapter = libraryAdapter;
		}

		public int GetCurrentPageId()
		{
			return GetCurrentItem().Id;
		}

		/// <summary>
		/// Checks the node isn't in a render template call.
		/// If it is then the id passed back is that of the parent document.
		/// </summary>
		public INode GetCurrentItem()
		{
			return _nodeAdapter.GetCurrent();
		}
		
		public INode GetItemById(int id)
		{
			_nodeAdapter.Load(id);
			return _nodeAdapter.Get();
		}

		public IList<ITag> GetAllTagsInGroup(string groupName)
		{
			return _tagAdapter.GetTags(groupName).OfType<ITag>().ToList();
		}

		public INode GetHomePageNode()
		{
			return GetItemById(-1).ChildrenAsList[0];
		}

		public Media GetMediaItem(int id)
		{
			return _mediaAdapter.Get(id);
		}

		public string GetNiceUrl(int nodeId)
		{
			return _libraryAdapter.NiceUrl(nodeId);
		}

		public INode GetFirstParentByAlias(string nodeTypeAlias)
		{
			var currentItem = GetCurrentItem();
			return GetFirstParentByAlias(nodeTypeAlias, currentItem.Id);
		}

		public INode GetFirstParentByAlias(string nodeTypeAlias, int nodeId)
		{
			var currentItem = GetItemById(nodeId);

			bool complete = false;
			while (!complete)
			{
				if (String.Compare(currentItem.NodeTypeAlias, nodeTypeAlias, StringComparison.OrdinalIgnoreCase) == 0)
				{
					try
					{
						return currentItem.Parent;
					}
					catch (NullReferenceException)
					{
						return null;
					}
					
				}
				try
				{
					currentItem = currentItem.Parent;
				}
				catch (NullReferenceException)
				{
					return null;
				}

				if (currentItem == null)
					complete = true;
			}

			return null;
		}

		public INode GetFirstParentByAlias(IList<string> nodeTypeAliases, int nodeId)
		{
			var currentItem = GetItemById(nodeId);

			bool complete = false;
			while (!complete)
			{
				if(nodeTypeAliases.Any(x=> String.Compare(currentItem.NodeTypeAlias, x, StringComparison.OrdinalIgnoreCase) == 0))
					return currentItem;

				currentItem = currentItem.Parent;

				if (currentItem == null)
					complete = true;
			}

			return null;
		}

		public IEnumerable<int> GetParentIdsForCurrentItem(int currentNodeId)
		{
			var currentItem = GetItemById(currentNodeId);
			if (currentItem == null)
				return new List<int>();

			return currentItem.Path
				.Split(new [] {','}, StringSplitOptions.RemoveEmptyEntries)
				.Select(int.Parse);
		}

		public IEnumerable<INode> GetChildNodes(int parentNodeId)
		{
			var parent = GetItemById(parentNodeId);

			if (parent == null)
				return new List<INode>();

			return parent.ChildrenAsList;
		}

		public IEnumerable<INode> GetChildNodes(int parentNodeId, string filterByNodeAlias)
		{
			return GetChildNodes(parentNodeId)
				.Where(x => String.Compare(x.NodeTypeAlias, filterByNodeAlias, StringComparison.OrdinalIgnoreCase) == 0);
		}

		public INode GetFirstChildByAlias(string nodeTypeAlias, int nodeId)
		{
			var children = GetChildNodes(nodeId);

			if (children == null)
				return null;

			return children.FirstOrDefault(x => string.Compare(x.NodeTypeAlias, nodeTypeAlias, StringComparison.OrdinalIgnoreCase) == 0);
		}

		public IEnumerable<INode> GetSiblings(int nodeId)
		{
			var parent = GetItemById(nodeId).Parent;

			return parent.ChildrenAsList.Where(x => x.Id != nodeId);
		}

		public IEnumerable<INode> GetSiblings(int nodeId, string filterByNodeAlias)
		{
			return GetSiblings(nodeId).Where(x => x.NodeTypeAlias == filterByNodeAlias);
		}

		public IEnumerable<INode> GetParentNodesForCurrentItem(string hompageNodeTypeAlias)
		{
			var parents = new List<INode>();
			var currentItem = GetCurrentItem();

			while (currentItem.Parent != null && currentItem.Parent.Id != -1)
			{
				parents.Add(currentItem.Parent);
				currentItem = currentItem.Parent;
			}

			parents.Reverse();

			return parents;
		}

		public IEnumerable<INode> GetDescendants(INode node)
		{
			foreach (var child in node.ChildrenAsList)
			{
				yield return child;

				foreach (var grandChild in GetDescendants(child))
				{
					yield return grandChild;
				}
			}
		}

		public string ParseUmbracoLocalLinks(string input)
		{
			if (!String.IsNullOrWhiteSpace(input))
			{
				var rxLink = new Regex("(/{localLink:)(.*)}");

				input = rxLink.Replace(input, delegate(Match m)
				{
					return _libraryAdapter.NiceUrl(int.Parse(rxLink.Replace(m.Value, "$2")));
				});
			}

			return input;
		}

		private string GetCustomProperty(string key)
		{
			return _libraryAdapter.GetHttpItem(key);
		}

	}
}
