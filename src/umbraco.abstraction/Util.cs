﻿namespace UmbracoMvcBlog.Umbraco.Abstraction
{
	using umbraco.BusinessLogic;

	public static class Util
	{
		/// <summary>
		/// Holds default values related with document types
		/// </summary>
		public static class DocumentTypeDefaultValues
		{
			/// <summary>
			/// Default value for DocumentType Icon
			/// </summary>
			public const string IconUrl = "folder.gif";

			/// <summary>
			/// Default value for DocumentType Thumbnail
			/// </summary>
			public const string Thumbnail = "folder.png";

			/// <summary>
			/// Name of the Generic properties tab (default and standard Umbraco tab which exists in every DocumentType)
			/// </summary>
			public const string TabGenericProperties = "Generic Properties";
		}

		public static User GetUmbracoAdminUser()
		{
			return new User(0);
		}
	}
}
