﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Providers
{
	using System;
	using System.Linq;
	using System.Runtime.Caching;
	using System.Web.Caching;
	using System.Collections.Generic;
	using umbraco.cms.businesslogic;
	using umbraco.cms.businesslogic.web;

	/// <summary>
	/// 
	/// </summary>
	/// <remarks></remarks>
	public class UmbracoByDeviceOutputCacheProvider : OutputCacheProvider 
	{

		/// <summary>
		/// 
		/// </summary>
		private readonly ObjectCache _cache = MemoryCache.Default;
		/// <summary>
		/// 
		/// </summary>
		private readonly static object _syncLock = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="T:System.Web.Caching.OutputCacheProvider"/> class.
		/// </summary>
		/// <remarks></remarks>
		public UmbracoByDeviceOutputCacheProvider()
		{
			//

			// Hook onto the umbraco publish event
			Document.AfterPublish += this.Document_AfterPublish;
		}

		/// <summary>
		/// Document_s the after publish.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="umbraco.cms.businesslogic.PublishEventArgs"/> instance containing the event data.</param>
		/// <remarks></remarks>
		void Document_AfterPublish(Document sender, PublishEventArgs e)
		{
			// A document has been published, purge the cache.
			PurgeCache();
		}

		/// <summary>
		/// Purges the cache.
		/// </summary>
		/// <remarks></remarks>
		public void PurgeCache()
		{
			List<string> cacheKeys = MemoryCache.Default.Select(kvp => kvp.Key).ToList();
			foreach (string cacheKey in cacheKeys)
			{
				if (!cacheKey.StartsWith("bl"))
					continue;

				MemoryCache.Default.Remove(cacheKey);
			}
			
		}

		/// <summary>
		/// Inserts the specified entry into the output cache.
		/// </summary>
		/// <param name="key">A unique identifier for <paramref name="entry"/>.</param>
		/// <param name="entry">The content to add to the output cache.</param>
		/// <param name="utcExpiry">The time and date on which the cached entry expires.</param>
		/// <returns>A reference to the specified provider.</returns>
		/// <remarks></remarks>
		public override object Add(string key, object entry, DateTime utcExpiry)
		{
			key = GetFullPathForKey(key);

			lock (_syncLock)
			{
				return _cache.AddOrGetExisting(key, entry, utcExpiry);
			}
		}

		/// <summary>
		/// Returns a reference to the specified entry in the output cache.
		/// </summary>
		/// <param name="key">A unique identifier for a cached entry in the output cache.</param>
		/// <returns>The <paramref name="key"/> value that identifies the specified entry in the cache, or null if the specified entry is not in the cache.</returns>
		/// <remarks></remarks>
		public override object Get(string key)
		{
			key = GetFullPathForKey(key);

			lock (_syncLock)
			{
				if (_cache.Contains(key)) return _cache[key];
			}

			return null;
		}

		/// <summary>
		/// Removes the specified entry from the output cache.
		/// </summary>
		/// <param name="key">The unique identifier for the entry to remove from the output cache.</param>
		/// <remarks></remarks>
		public override void Remove(string key)
		{
			key = GetFullPathForKey(key);

			lock (_syncLock)
			{
				_cache.Remove(key);
			}
		}

		/// <summary>
		/// Inserts the specified entry into the output cache, overwriting the entry if it is already cached.
		/// </summary>
		/// <param name="key">A unique identifier for <paramref name="entry"/>.</param>
		/// <param name="entry">The content to add to the output cache.</param>
		/// <param name="utcExpiry">The time and date on which the cached <paramref name="entry"/> expires.</param>
		/// <remarks></remarks>
		public override void Set(string key, object entry, DateTime utcExpiry)
		{
			key = GetFullPathForKey(key);

			// CG: Whats this for then?
		}

		/// <summary>
		/// Gets the full path for key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		/// <remarks></remarks>
		private string GetFullPathForKey(string key)
		{
			return string.Format("bl-{0}-{1}",
				"Mobile",
				key.Replace('/', '$').ToLower()
			);
		}

	}
}
