﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters
{
	/// <summary>
	/// This is an adapter for the umbraco.cms.businesslogic.web.Document class
	/// Using this adapter instead of the concrete class allows you to unit test
	/// any code that would have been dependent on Document. 
	/// </summary>
	public interface IDocumentAdapter
	{
		void LoadDocument(int documentId);
		void Save();
		void SaveAndPublish(int userId, bool updateDocumentCache);

	}
}
