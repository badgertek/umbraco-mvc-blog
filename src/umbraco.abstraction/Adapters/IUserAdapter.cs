﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters
{
	using umbraco.BusinessLogic;

	/// <summary>
	/// This is an adapter for the umbraco.businesslogic.User class
	/// Using this adapter instead of the concrete class allows you to unit test
	/// any code that would have been dependent on User. 
	/// </summary>
	public interface IUserAdapter
	{
		void Load(int userId);
		User Get();

	}
}
