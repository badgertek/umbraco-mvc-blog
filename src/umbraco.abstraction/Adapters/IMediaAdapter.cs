﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters
{
	using umbraco.cms.businesslogic.media;

	/// <summary>
	/// This is an adapter for the umbraco.NodeFactory.Node class
	/// Using this adapter instead of the concrete class allows you to unit test
	/// any code that would have been dependent on Node. 
	/// </summary>
	public interface IMediaAdapter
	{
		Media Get(int mediaId);

	}
}
