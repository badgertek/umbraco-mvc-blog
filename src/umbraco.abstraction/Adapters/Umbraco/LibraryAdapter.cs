﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters.Umbraco
{
	public class LibraryAdapter : ILibraryAdapter
	{
		public void UpdateDocumentCache(int documentId)
		{
			umbraco.library.UpdateDocumentCache(documentId);
		}

		public string NiceUrl(int nodeId)
		{
			return umbraco.library.NiceUrl(nodeId);
		}

		public string GetHttpItem(string key)
		{
			return umbraco.library.GetHttpItem(key);
		}
	}
}