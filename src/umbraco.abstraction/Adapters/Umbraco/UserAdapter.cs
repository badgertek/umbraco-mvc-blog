﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters.Umbraco
{
	using umbraco.BusinessLogic;

	public class UserAdapter : IUserAdapter
	{

		private User User { get; set; }

		public void Load(int userId)
		{
			User = new User(userId);
		}

		public User Get()
		{
			return User;
		}
	}
}