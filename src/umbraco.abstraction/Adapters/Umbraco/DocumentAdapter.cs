﻿
namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters.Umbraco
{
	using System;
	using umbraco.cms.businesslogic.web;

	/// <summary>
	/// This is an adapter for the umbraco.cms.businesslogic.web.Document class
	/// Using this adapter instead of the concrete class allows you to unit test
	/// any code that would have been dependent on Document. 
	/// </summary>
	public class DocumentAdapter : IDocumentAdapter
	{
		private Document _document;
		private readonly IUserAdapter _userAdapter;
		private readonly ILibraryAdapter _libraryAdapter;

		public DocumentAdapter(IUserAdapter userAdapter, ILibraryAdapter libraryAdapter)
		{
			if(userAdapter == null)
				throw new ArgumentNullException("userAdapter");

			if(libraryAdapter == null)
				throw new ArgumentNullException("libraryAdapter");

			_userAdapter = userAdapter;
			_libraryAdapter = libraryAdapter;
		}

		public void LoadDocument(int documentId)
		{
			_document = new Document(documentId);
		}

		public void Save()
		{
			_document.Save();
		}

		public void SaveAndPublish(int userId, bool updateDocumentCache)
		{
			_userAdapter.Load(userId);
			Save();
			_document.Publish(_userAdapter.Get());

			if (updateDocumentCache)
				_libraryAdapter.UpdateDocumentCache(_document.Id);
		}
	}
}
