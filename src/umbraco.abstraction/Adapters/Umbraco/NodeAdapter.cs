﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters
{
	using umbraco.NodeFactory;
	using umbraco.interfaces;

	public class NodeAdapter : INodeAdapter
	{
		private INode Node { get; set; }

		public void Load(int nodeId)
		{
			Node = new Node(nodeId);
		}

		public INode Get()
		{
			return Node;
		}

		public INode GetCurrent()
		{
			Node = umbraco.NodeFactory.Node.GetCurrent();
			return Node;
		}
	}
}