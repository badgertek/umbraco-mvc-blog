﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters
{
	using System.Collections.Generic;
	using umbraco.cms.businesslogic.Tags;
	using umbraco.interfaces;

	public class TagAdapter : ITagAdapter
	{
		public IEnumerable<ITag> GetTags(string groupName)
		{
			return Tag.GetTags(groupName);
		}
	}
}