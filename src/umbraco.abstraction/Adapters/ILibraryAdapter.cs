﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters
{
	public interface ILibraryAdapter
	{
		void UpdateDocumentCache(int documentId);
		string NiceUrl(int nodeId);
		string GetHttpItem(string key);
	}
}
