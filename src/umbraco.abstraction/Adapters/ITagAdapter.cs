﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters
{
	using System.Collections.Generic;
	using umbraco.interfaces;

	/// <summary>
	/// This is an adapter for the umbraco.cms.businesslogic.Tags.Tag class
	/// Using this adapter instead of the concrete class allows you to unit test
	/// any code that would have been dependent on Tag. 
	/// </summary>
	public interface ITagAdapter
	{
		IEnumerable<ITag> GetTags(string groupName);
	}
}
