﻿namespace UmbracoMvcBlog.Umbraco.Abstraction.Adapters
{
	using umbraco.interfaces;

	/// <summary>
	/// This is an adapter for the umbraco.NodeFactory.Node class
	/// Using this adapter instead of the concrete class allows you to unit test
	/// any code that would have been dependent on Node. 
	/// </summary>
	public interface INodeAdapter
	{
		void Load(int nodeId);
		INode Get();
		INode GetCurrent();
	}
}
