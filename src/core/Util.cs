﻿namespace UmbracoMvcBlog.Core
{
	using System;
	using System.Collections.Generic;
	using System.Reflection;
	using umbraco.BusinessLogic;

	internal static class Util
	{
		/// <summary>
		/// Get's the attribute of a given type from the given type.
		/// Note that if there are multiple attributes of the same type found, this method returns on the first one
		/// so use this method only for searching attributes whose AllowMultiple is set to false.
		/// </summary>
		/// <typeparam name="T">Name of the attribute class</typeparam>
		/// <param name="type">Type whose attributes will be searched</param>
		/// <returns>Attribute found or null of attribute is not found</returns>
		public static T GetAttribute<T>(Type type)
		{
			T retVal;

			object[] attributes = type.GetCustomAttributes(typeof(T), true);
			if (attributes.Length > 0)
			{
				retVal = (T)attributes[0];
			}
			else
			{
				retVal = default(T);
			}

			return retVal;
		}

		/// <summary>
		/// Get's all subtypes of a given type.
		/// </summary>
		public static List<Type> GetAllSubTypes(Type type)
		{
			List<Type> retVal = new List<Type>();

			Assembly[] assemblies = GetSiteBuilderAssemblies();

			// NOTE: The similar functionality is located in the method above
			foreach (Assembly assembly in assemblies)
			{
				Module[] modules = assembly.GetLoadedModules();

				foreach (Module module in modules)
				{
					try
					{
						Type[] types = null;
						types = module.GetTypes();
						foreach (Type t in types)
						{
							if (t.BaseType != null && t.IsSubclassOf(type))
							{
								retVal.Add(t);
							}
						}
					}
					catch { } // required because Exception is thrown for some dlls when .GetTypes method is called
				}
			}

			return retVal;
		}

		private static Assembly[] GetSiteBuilderAssemblies()
		{
			//if (USiteBuilderConfiguration.Assemblies != "")
			//{
			//	string[] assemblynames = USiteBuilderConfiguration.Assemblies.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

			//	List<Assembly> result = new List<Assembly>();

			//	foreach (string name in assemblynames)
			//	{
			//		// This will throw an exception if we spelled it wrong.
			//		result.Add(Assembly.Load(name));
			//	}

			//	result.Add(Assembly.GetExecutingAssembly());

			//	return result.ToArray();
			//}
			//else
				return AppDomain.CurrentDomain.GetAssemblies();
		}

		public static bool IsGenericArgumentTypeOf(Type typeGeneric, Type typeArgument)
		{
			foreach (Type t in typeGeneric.GetGenericArguments())
			{
				if (t.Equals(typeArgument)) return true;
			}

			if (typeGeneric.BaseType != null)
			{
				return IsGenericArgumentTypeOf(typeGeneric.BaseType, typeArgument);
			}
			else
			{
				return false;
			}
		}
	}
}
