﻿namespace UmbracoMvcBlog.Core.Services
{
	public interface IDocumentTypeService
	{
		void CreateDocumentTypes();
	}
}
