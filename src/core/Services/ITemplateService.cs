﻿namespace UmbracoMvcBlog.Core.Services
{
	using System;
	using System.Collections.Generic;

	public interface ITemplateService
	{
		IList<Type> GetAll(Type documentType);

	}
}
