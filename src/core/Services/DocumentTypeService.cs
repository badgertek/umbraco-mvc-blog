﻿namespace UmbracoMvcBlog.Core.Services
{
	using System;
	using Domain.Models;
	using Repositories;

	public class DocumentTypeService : IDocumentTypeService
	{

		private readonly IDocumentTypeRepository _documentTypeRepository;

		public DocumentTypeService(IDocumentTypeRepository documentTypeRepository)
		{
			if(documentTypeRepository == null)
				throw new ArgumentNullException("documentTypeRepository");

			_documentTypeRepository = documentTypeRepository;
		}

		public void CreateDocumentTypes()
		{
			_documentTypeRepository.Create(typeof(UmbracoMvcBlogPost));
		}
	}
}