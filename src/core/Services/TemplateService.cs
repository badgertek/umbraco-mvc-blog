﻿namespace UmbracoMvcBlog.Core.Services
{
	using System;
	using System.Collections.Generic;
	using Repositories;

	public class TemplateService : ITemplateService
	{
		private readonly ITemplateRepository _templateRepository;

		public TemplateService(ITemplateRepository templateRepository)
		{
			if(templateRepository == null)
				throw new ArgumentNullException("templateRepository");

			_templateRepository = templateRepository;
		}
	
		public IList<Type> GetAll(Type documentType)
		{
			return _templateRepository.GetAll(documentType);
		}
	}
}