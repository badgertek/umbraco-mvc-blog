umbraco-mvc-blog
================

Feature rich blog package for Umbraco 4.11 + built using MVC.

#Feature Rich?#
Well, hopefully it will be! It's early days but we hope to build a full featured blog package for Umbraco with an easy to use API, sensible set of MVC default views (No XSLT Macros to be found here).

##Features we'd like to implement##

* Auto structuring of articles in back office (by date or category)
* Categories
* Tags
* Comments (Disquss)
* RSS/Atom feeds
* Pingbacks
* Examine based search
* Request and Application level caching

#Building the package#
If you would like to clone the source and build the package for yourself, you can run /src/buildpackage/BuildPackage.cmd which will generate an installable Umbraco Package (NuGet) file.
